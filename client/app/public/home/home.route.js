import template from './home.html';
import controller from './home.controller';

function routeConfig($stateProvider) {
	'ngInject';

	$stateProvider
		.state('app.public.home', {
			url: '/home',
			template,
			controller,
			controllerAs: 'vm'
		});
}

export default routeConfig;