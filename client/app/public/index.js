import routeConfig from './public.route';
import publicLocale from './public.locale';

angular.module('Boilerplate')
	.config(routeConfig)
	.config(publicLocale);
	
require('./home');