import template from './public.html';
import controller from './public.controller';

function routeConfig($stateProvider) {
	'ngInject';

	$stateProvider
		.state('app.public', {
			abstract: true,
			template,
			controller,
			controlerAs: 'vm'
		});
}

export default routeConfig;