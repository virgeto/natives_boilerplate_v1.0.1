function PublicController() {
	'ngInject';
	
	const vm = this;
	vm.layoutSetup = layoutSetup;
	
	layoutSetup();
	angular.element( window ).on('resize', function() {
		layoutSetup();
	});
	
	/** */
	function layoutSetup() {
		const body = document.getElementsByTagName('body')[0];		
		const aside = document.getElementById('public__container');
		const windowHeight = document.documentElement.clientHeight;
		
		body.style.height = `${ windowHeight }px`;
		aside.style.minHeight = `${ windowHeight - 80 }px`;
	}
}

export default PublicController;