export default function ($translateProvider) {
	'ngInject';

	$translateProvider
		.translations('bg', {
		})
		.translations('en', {
		});
		
	$translateProvider.useSanitizeValueStrategy(null);
	$translateProvider.use('bg');
}