import template from './login.html';
import controller from './login.controller';

function routeConfig($stateProvider) {
	'ngInject';

	$stateProvider
		.state('app.private.login', {
			url: '/login',
			template,
			controller,
			controllerAs: 'vm'
		});
}

export default routeConfig;