import routeConfig from './private.route';

angular.module('Boilerplate')
	.config(routeConfig);

require('./main');
require('./login');
