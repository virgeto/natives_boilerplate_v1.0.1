import template from './list.html';
import controller from './list.controller';

function routeConfig($stateProvider) {
	'ngInject';

	$stateProvider
		.state('app.private.main.example.list', {
			url: '/list',
			template,
			controller,
			controllerAs: 'vm'
		});
}

export default routeConfig;