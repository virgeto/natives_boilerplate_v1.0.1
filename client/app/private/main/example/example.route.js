function routeConfig($stateProvider) {
	'ngInject';

	$stateProvider
		.state('app.private.main.example', {
			abstract: true,
			url: '/example',
			'template': '<div ui-view></div>'
		});
}

export default routeConfig;