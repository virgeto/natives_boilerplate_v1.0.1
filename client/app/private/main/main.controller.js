function MainController() {
	'ngInject';
	
	const vm = this;
	vm.visibleSidebar = false; 
	vm.showSidebar = showSidebar;
	vm.showVersion = false;
	
	/** */
	function showSidebar(bool) {
		const body = angular.element(document.getElementsByTagName('BODY')[0]);
		bool ? body.addClass('scroll--locked') : body.removeClass('scroll--locked');
		vm.visibleSidebar = bool;
	}
	
}

export default MainController;