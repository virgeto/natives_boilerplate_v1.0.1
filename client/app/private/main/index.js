import routeConfig from './main.route';

angular.module('Boilerplate')
	.config(routeConfig);

require('./example');
