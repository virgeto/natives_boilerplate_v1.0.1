import template from './main.html';
import controller from './main.controller';

function routeConfig($stateProvider) {
	'ngInject';

	$stateProvider
		.state('app.private.main', {
			abstract: true,
			template,
			controller,
			controllerAs: 'vm'
		});
}

export default routeConfig;