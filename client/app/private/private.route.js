function routeConfig($stateProvider) {
	'ngInject';

	$stateProvider
		.state('app.private', {
			url: '/admin',
			template: '<div ui-view></div>'
		});
}

export default routeConfig;