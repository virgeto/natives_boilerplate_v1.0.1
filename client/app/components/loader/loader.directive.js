import LoaderTemplate from './loader.html';

function Loader() {
	'ngInject';

	return {
		'restrict': 'E',
		'template': LoaderTemplate,
		'scope': {
			'visible': '@'
		},
		'link': function (scope) {
			scope.visible = true;
		}
	};
}

export default Loader;