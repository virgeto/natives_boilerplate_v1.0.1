import Loader from './loader.directive';
import LoaderComponent from './loader.service';

angular.module('Components')
	.directive('loader', Loader)
	.factory('LoaderComponent', LoaderComponent);