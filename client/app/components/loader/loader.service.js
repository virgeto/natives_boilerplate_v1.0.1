function LoaderComponent() {
	'ngInject';
	
	const service = {
		'body': angular.element(document.getElementsByTagName('body')),
		'loader': angular.element(document.getElementsByTagName('loader')),
		'spinner': spinner
	};
	
	return service;
	
	/** */
	function spinner( trigger ) {
		if ( trigger === 'on' ) {
			service.body.css({
				'overflow': 'hidden',
				'width': '100%',
				'height': '100vh'
			});
			service.loader.css({
				'position': 'fixed',
				'top': 0,
				'z-index': 10000000
			});
		} else if ( trigger === 'off' ) {
			service.body.css({
				'overflow': 'auto',
				'width': '100%',
				'height': 'auto'
			});
			service.loader.css({
				'top': '-999rem'
			});
		}
	}
}

export default LoaderComponent;