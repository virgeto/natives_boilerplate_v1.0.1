import SidebarTemplate from './sidebar.html';

function Sidebar(Auth, $location, ConfirmDialogComponent, $filter) {
	'ngInject';
	
	return {
		restrict: 'E',
		template: SidebarTemplate,
		link: function (scope) {
			scope.visible = false;
			scope.logout = logout;	
			scope.menuTriger = menuTriger;
			scope.slimMenu = false; 
			scope.$watch(function() {
				scope.currentRoute = function(routeName) {				
					if ( $location.path().indexOf(routeName) !== -1) {
						return true;
					} else {
						return false;
					}
				};
			});
			scope.categories = [
				{
					'icon': 'fa-user',
					'name': 'test',
					'link': 'app.private.main.example.list'
				},
				{
					'icon': 'fa-users',
					'name': 'test',
					'link': 'app.private.main.example.list'
				}
			];
			
			/** Logout user fron Auth service*/
			function logout() {
				if (confirm($filter('translate')('MESSAGES.SURE'))) {
					Auth.logout();
				}
			}
			
			/** */
			function menuTriger() {
				scope.slimMenu = !scope.slimMenu;
			}
			
		}
	};
}

export default Sidebar;