import Sidebar from './sidebar.directive';

angular.module('Components')
	.directive('sidebar', Sidebar);