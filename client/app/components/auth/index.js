import auth from './auth.service';

angular.module('Components')
	.factory('Auth', auth);
