function Auth(LoaderComponent, $http, $window, AppConstants, $cookieStore, $state, toastr, $q) {
	'ngInject';

	const service = {
		login,
		userLogin,
		userLogout,
		logout,
		getCurrentUser
	};

	let currentUser = {};

	return service;
	
	/** Login  
	 * @params { Object } user
	 * 	email user.email
	 * 	password: user.password
	*/

	function login(user) {
		LoaderComponent.spinner('on');
		
		return $http.post(`${AppConstants.apiUrl}/auth-admin`, user)
			.then(success)
			.catch(failedRequest);
	}

	/** LogOut user ad clead token from cookies */
	function logout() {
		$cookieStore.remove('token');
		$state.go('app.private.login');
	}
	
	/** */
	function userLogin( credentials ) {
		LoaderComponent.spinner('on');
		
		return $http.post(`${ AppConstants.apiUrl}/auth-user`, credentials)
			.then(( response ) => {
				if ( response.data.status !== 'error' ) {
					const res = response.data.data;
					
					$cookieStore.put('token', res.token);
					sessionStorage.setItem('active', res.user.id);
					LoaderComponent.spinner('off');
					
					return res;
				} else {
					toastr.error(response.data.message);	
					LoaderComponent.spinner('off');		
				}
				
			})	
			.catch(( error ) => {
				return error;
			});
		
	}
	
	/** */
	function userLogout() {
		const deferred = $q.defer();
		
		sessionStorage.removeItem('active');
		deferred.resolve($cookieStore.remove('token'));
		
		
		return deferred.promise;
	}

	/** Succesful callback
	 * @param { Object } response
	 * return { Object } response
	 */
	function success(data) {
		if ( data.data.status !== 'error' ) {
			const response = data.data.data;

			if (response.token) {
				$cookieStore.put('token', response.token);
				setCurrentUser(data); 
			} 
		}
		LoaderComponent.spinner('off');		

		return data;
	}

	/** Error handler on call back
	 *	@param { Object } response
	 */
	function failedRequest(data) {
		LoaderComponent.spinner('off');		
		return data;
	}
	
	/** Set current user if login success
	 * @params { Object } data
	*/
	function setCurrentUser(data) {
		currentUser = data.data.data.user;
		return currentUser;
	}
	
	/** Current user getter
	 * return { Object } currentUser
	*/
	function getCurrentUser() {
		return currentUser;
	}
}

export default Auth;
