function UserComponent($http, AppConstants) {
	'ngInject';
	
	const service = {
		isLoggedAsync,
		changePassword
		
	};
	
	let newPassword = {};

	return service;
	
	/** */
	function isLoggedAsync() {
		return $http.get(`${AppConstants.apiUrl}/me`)
			.then(function() {		
			})
			.catch(failedRequiest);
	}
	
	/** */
	function changePassword(params) {
		return $http.put(`${AppConstants.apiUrl}/change-password`, params)
			.then(success)
			.catch(failedRequiest);
	}
	
	/** */
	function success(data) {
		newPassword = data.data.data;
		return newPassword;
	}
	
	/** */
	function failedRequiest(err) {
		return err;
	}
}

export default UserComponent;