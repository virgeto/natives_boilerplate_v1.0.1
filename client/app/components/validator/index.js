import ValidatorService from './validator.service';

angular.module('Components')
	.service('ValidatorService', ValidatorService);