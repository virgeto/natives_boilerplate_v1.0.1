function ValidatorService(toastr, $state, $filter) {
	'ngInject';
	
	const service = {
		formValidator,
		submitValidator
	};

	return service;
	
	/** */
	function formValidator(form) {
		let isValidForm = false;

		form.$invalid ? isValidForm = false : isValidForm = true;

		return isValidForm;
	}
	
	/** */
	function submitValidator(error, action, type) {
		if (error) {
			angular.forEach(error, function(err) {
				toastr.error(err[0]);
			});
		} else {
			toastr.success($filter('translate')(`${ type.toUpperCase() }.${ action.toUpperCase() }_SUCCESS`));
			$state.go(`app.private.main.${ type }.list`);
		}
	}
}

export default ValidatorService;