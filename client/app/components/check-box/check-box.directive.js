import CheckBoxTemplate from './check-box.html';

function CheckBox() {
	'ngInject';

	return {
		restrict: 'E',
		'template': CheckBoxTemplate,
		'transclude': true,
		'replacce': true,
		'scope': {
			'model': '='
		}
	};
}

export default CheckBox;