import CheckBox from './check-box.directive';

angular.module('Components')
	.directive('checkBox', CheckBox);