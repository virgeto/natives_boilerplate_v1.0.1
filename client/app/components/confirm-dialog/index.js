import ConfirmDialog from './confirm-dialog.directive';
import ConfirmDialogComponent from './confirm-dialog.service';

angular.module('Components')
	.directive('confirmDialog', ConfirmDialog)
	.factory('ConfirmDialogComponent', ConfirmDialogComponent);