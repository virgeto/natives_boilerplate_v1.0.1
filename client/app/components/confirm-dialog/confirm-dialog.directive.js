import ConfirmDialogTemplate from './confirm-dialog.html';

function ConfirmDialog() {
	'ngInject';

	return {
		'restrict': 'E',
		'template': ConfirmDialogTemplate,
		'transclude': true,
		'scope': {
			'action': '='
		},
		'link': function () {
		}
	};
}

export default ConfirmDialog;