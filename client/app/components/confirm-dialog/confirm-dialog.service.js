function ConfirmDialogComponent() {
	
	const service = {
		confirmation
	};

	return service;

	/** */
	function confirmation(cb) {
		const element = angular.element(document.getElementById('confirm__wrapper'));
		element.attr('visible', '1');
		element.parent().attr('action', cb);
	}
}

export default ConfirmDialogComponent;