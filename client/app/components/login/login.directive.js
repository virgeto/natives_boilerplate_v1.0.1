import LoginTemplate from './login.html';

function Login(Auth, $state, toastr, $filter) {
	'ngInject';

	return {
		restrict: 'E',
		'template': LoginTemplate,
		'transclude': true,
		'scope': {
			'linkSuccess': '@'
		},
		'link': function (scope) {
			scope.login = login;
			scope.clear = false;
	
			function login(user) {
				let isValidForm = true;
				scope.clear = true;
				
				if (scope.loginForm.$invalid) {
					isValidForm = false;
				}
				
				if (isValidForm) {
					Auth.login(user)
					.then(function(data) {
						if (data.data.status === 'error') {
							toastr.error($filter('translate')(`ERROR.CODE_${ data.data.error_code }`));
						} else {
							$state.go(scope.linkSuccess);
						}
					});
				} else {
					toastr.error($filter('translate')('MESSAGES.WENT_WRONG'));
					return false;
				}
			}
		}
	};
}

export default Login;