import TopMenuTemplate from './top-menu.html';

function TopMenu(AppConstants, $timeout) {
	'ngInject';
	
	return {
		restrict: 'E',
		'template': TopMenuTemplate,
		'replace': true,
		'scope': {},
		'link': function(scope) {
			const body = document.getElementsByTagName('body')[0];
			scope.mobileMenu = false;
			scope.languages = AppConstants.languages;
			scope.menuItems = [
				{
					'title': 'Home',
					'state': 'app.public.home'
				},
				{
					'title': 'Packages',
					'state': 'app.public.packages'
				},
				{
					'title': 'Channels',
					'state': 'app.public.channels'
				}
			];
			
			scope.$watch(function() {
				scope.isLogged = sessionStorage.getItem('active') ? true : false;
			});
			
			scope.hideMobileMenu = function() {
				scope.mobileMenu = false;
				unlockBody();
			};
			
			scope.mobileTrigger = function () {
				scope.mobileMenu = !scope.mobileMenu;
				
				if (scope.mobileMenu) {
					body.style.height = `${ window.innerHeight }px`;
					body.style.overflow = `hidden`;
					
					document.ontouchmove = function(e) { e.preventDefault(); };		
					
					setTimeout(function() {
						const mobileItem = document.querySelectorAll('.mobile-menu__item');
						showMenuItem( mobileItem );
					}, 200);
					
				} else {
					unlockBody();
				}
			};
			
			/** */
			angular.element( window ).on('resize', function () {
				if ( window.innerWidth < 980 ) scope.mobileMenu = false;
				unlockBody();
				scope.$digest();
			});
			
			/** Async show items with delay
			 * @params { Array } - items
			 * @retrn {  } 
			 */
			function showMenuItem( items ) {
				let promise = $timeout();
				
				setTimeout(function() {
					for ( let i = 0, k = items.length; i < k; i++ ) {
						promise = promise.then(() => {
							angular.element(items[i]).addClass('slide--async');
							
							return $timeout(100);
						});
					}
				}, 200);
			}
			
			/** */
			function unlockBody() {
				document.ontouchmove = function() { return true; };
				
				body.style.height = `${ window.innerHeight }px`;					
				body.style.overflow = `auto`;
			}
			
		}
	};
}

export default TopMenu;