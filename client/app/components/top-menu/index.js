import TopMenu from './top-menu.directive';

angular.module('Components')
	.directive('topMenu', TopMenu);