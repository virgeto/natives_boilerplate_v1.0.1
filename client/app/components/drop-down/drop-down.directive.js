import DropDownTemplate from './drop-down.html';

function DropDown() {
	'ngInject';

	return {
		restrict: 'E',
		'template': DropDownTemplate,
		'transclude': true,
		'scope': {
			'model': '=',
			'query': '@',
			'search': '=',
			'name': '@',
			'placeholder': '@',
			'clear': '=',
			'required': '='
		},
		'link': function (scope) {
			scope.results = [];
			scope.query = '';
			scope.showResults = false;
			scope.searchResults = searchResults;
			scope.selectItem = selectItem;
			scope.resultsContainer = {};
			
			activate();
			
			scope.$watch(function() {
				if ( scope.clear ) {
					scope.query = '';	
				}
			});
			
			document.addEventListener('keydown', function (event) {
				switch (event.which) {
					case 27:
						scope.resultsContainer[1].style.display = 'none';
						break;
					default:
				}

			});
			
			document.addEventListener('click', function(event) {
				const clickedElement = angular.element(event.target);
				const bool = clickedElement.hasClass('drop-down__input');

				if ( !bool && scope.resultsContainer[1] ) {
					scope.resultsContainer[1].style.display = 'none';					
				}
			});
			
			/** */
			function activate() {
				clear();
			}
			
			/** */
			function searchResults(event) {
				scope.resultsContainer = angular.element(event.target).parent().children('.drop-down__container--results');
				scope.resultsContainer.css({ 'display': 'block' });
				
				if ( scope.results.length === 0 ) {
					scope.search()
						.then(function(data) {
							scope.results = data;						
						});
				}
			}
			
			/** */
			function selectItem(item) {
				if ( typeof scope.clear !== 'undefined' ) {
					scope.clear = false;
				}
				
				if (!isNaN(parseInt(item, 10))) {
					scope.query = item;
					scope.model = item;
				} else {
					scope.query = item.name || item.names;
					scope.model = item;
				}

				scope.resultsContainer[1].style.display = 'none';
				
			}
			
			
			/** */
			function clear() {
				scope.results = [];
				scope.query = '';
			}
		}
	};
}

export default DropDown;