import DropDown from './drop-down.directive';

angular.module('Components')
	.directive('dropDown', DropDown);