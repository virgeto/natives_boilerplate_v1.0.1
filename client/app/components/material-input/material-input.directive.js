import MaterialInputTemplate from './material-input.html';

function MaterialInput() {
	'ngInject';
	
	return {
		'restrict': 'E',
		'template': MaterialInputTemplate,
		'transclude': true,
		'scope': {
			'inputClass': '@',
			'model': '=',
			'type': '@',
			'name': '@',
			'required': '@',
			'labelText': '@'
		},
		'link': function() {
		}
	};
}

export default MaterialInput;