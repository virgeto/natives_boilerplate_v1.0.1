import MaterialInput from './material-input.directive';

angular.module('Components')
	.directive('materialInput', MaterialInput);