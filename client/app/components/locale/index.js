import Locale from './locale.directive';

angular.module('Components')
	.directive('locale', Locale);