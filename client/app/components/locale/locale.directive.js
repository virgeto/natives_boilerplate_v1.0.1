import LocaleTemplate from './locale.html';

function Locale() {
	'ngInject';

	return {
		restrict: 'E',
		'template': LocaleTemplate,
		'transclude': true,
		'scope': {
			'locale': '@'
		}
	};
}

export default Locale;