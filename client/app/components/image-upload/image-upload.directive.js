import ImageUploadTemplate from './image-upload.html';

function ImageUpload() {
	'ngInject';
	
	return {
		restrict: 'E',
		template: ImageUploadTemplate,
		scope: {
			'imageModel': '='
		},
		link: function () {
		}
	};
}

export default ImageUpload;