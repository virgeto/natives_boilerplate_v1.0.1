import ImageUpload from './image-upload.directive';

angular.module('Components')
	.directive('imageUpload', ImageUpload);
