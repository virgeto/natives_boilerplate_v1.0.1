import PackageComponent from './package.service';

angular.module('Components')
	.factory('PackageComponent', PackageComponent);