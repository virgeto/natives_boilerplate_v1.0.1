function PackageComponent(AppConstants, $http) {
	'ngInject';
	
	const service = {
		fetchPackages,
		createPackage,
		showPackage,
		updatePackage,
		deletePackage,
		getPackages
	};
	
	let packages = {};

	return service;
	
	/** */
	function fetchPackages() {
		return $http.get(`${AppConstants.apiUrl}/packages`)
			.then(success)
			.catch(failedRequest);
	}
	
	/** */
	function createPackage(params) {
		return $http.post(`${ AppConstants.apiUrl }/packages`, params)
			.then(success)
			.catch(failedRequest);
	}
	
	/** */
	function showPackage(id) {
		return $http.get(`${AppConstants.apiUrl}/packages/${id}`)
			.then(success)
			.catch(failedRequest);
	}
	
	/** */
	function updatePackage(id, params) {
		return $http.put(`${ AppConstants.apiUrl }/packages/${id}`, params )
			.then(success)
			.catch(failedRequest);
	}
	
	/** */
	function deletePackage(id) {
		return $http.delete(`${AppConstants.apiUrl}/packages/${id}`)
			.then(service.fetchPackages)
			.catch(failedRequest);
	}
	
	/** */
	function success(data) {
		if (data.data.errors) {
			return data.data;
		} else {
			packages = data.data.data;		
			return packages;
		}
	}
	
	/** */
	function failedRequest(data) {
		console.log(data);
	}
	
	/** */
	function getPackages() {
		return packages;
	}
}

export default PackageComponent;