import SectionLabel from './section-label.directive';

angular.module('Components')
	.directive('sectionLabel', SectionLabel);