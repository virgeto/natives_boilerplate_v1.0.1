import SectionLabelTemplate from './section-label.html';

function SectionLabel() {
	'ngInject';
	
	return {
		restrict: 'E',
		'template': SectionLabelTemplate,
		'transclude': true,
		'scope': {
			'backButtonLink': '@',
			'addButtonLink': '@'
		}
	};
}

export default SectionLabel;