import authInterceptor from './auth.interceptor';

angular.module('Components')
	.service('authInterceptor', authInterceptor);
