function authInterceptor($cookieStore, AppConstants) {
	'ngInject';

	const service = {
		request: request
	};

	return service;

	function request(config) {
		const isАllowable = config.url === `${ AppConstants.apiUrl }/auth-user` || config.url === `${ AppConstants.apiUrl }/auth-admin` ? false : true; 

		const token = $cookieStore.get('token');
		const version = 1;
		
		config.headers = config.headers || {};
		config.headers.Version = version;
		config.headers.Accept = 'application/json';
		config.headers['Accept-Language'] = 'bg';
		if ( $cookieStore.get('token') && isАllowable ) {
			config.url = transformUrl(config.url, token);
		}
		
		return config;
	}

	/** */
	function transformUrl(url, token) {
		// Check if the request is for a template
		const templateTest = new RegExp('(http:\/\/)|(https:\/\/)');
		if (!templateTest.test(url)) {
			return url;
		}

		// Check if the token is the first query param or not
		const urlTest = new RegExp('&|\\?');
		if (urlTest.test(url)) {
			return url + '&token=' + token;
		} else {
			return url + '?token=' + token;
		}
	}
}

export default authInterceptor;

