import ImageUploadBase64Template from './image-upload-base64.html';

function ImageUploadBase64() {
	'ngInject';
	
	return {
		restrict: 'E',
		template: ImageUploadBase64Template,
		scope: {
			'imageModel': '=',
			'query': '@',
			'type': '@'
		},
		link: function (scope) {
			console.log(scope.imageModel);
			scope.imageChange = imageChange;
			scope.deleteImage = deleteImage;
			scope.multipleImages = [];
			scope.type = scope.type === 'multiple' ? 'multiple' : 'single';  
			
			/** */
			function imageChange(image) {
				console.log(scope.type);
				if ( scope.type === 'multiple' ) {
					multipleImages(image);
				}
			}
			
			/** */
			function multipleImages(image) {
				for ( let i = 0, k = image.length; i < k; i++ ) {
					const index = imageIndex(scope.multipleImages, image[i].filename);
					
					if (index === -1) {
						scope.multipleImages.push(image[i]);
					} 

				}
				scope.imageModel = scope.multipleImages;
			}
			
			/** */
			function deleteImage(imageName) {
				const index = imageIndex(scope.multipleImages, imageName);
				console.log(index);
				
				if ( index !== -1 ) {
					scope.multipleImages.splice(index, 1);
				}

				return scope.imageIndex;
			}
			
			/** */
			function imageIndex(images, imageName) {
				const imgIndex = images.map(function(e) { return e.filename; }).indexOf(imageName);
				
				return imgIndex;
			}
		}
	};
}

export default ImageUploadBase64;