import ImageUploadBase64 from './image-upload-base64.directive';

angular.module('Components')
	.directive('imageUploadBase64', ImageUploadBase64);
