import Autocomplete from './autocomplete.directive';

angular.module('Components')
	.directive('autocomplete', Autocomplete);