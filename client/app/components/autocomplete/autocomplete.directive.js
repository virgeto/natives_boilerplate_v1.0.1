import AutocompleteTemplate from './autocomplete.html';

function Autocomplete($timeout) {
	'ngInject';

	return {
		restrict: 'E',
		'template': AutocompleteTemplate,
		'transclude': true,
		'scope': {
			'model': '=',
			'search': '=' 
		},
		'link': function (scope, element) {
			const elementResults = element.find('section');
			const elementSpinner = element.find('img');
			scope.results = [];
			scope.query = '';
			scope.searchResults = searchResults;
			scope.selectItem = selectItem;
			scope.focusLost = focusLost;
			
			document.addEventListener('keydown', function (event) {
				switch (event.which) {
					case 27:
						elementResults[0].style.display = 'none';
						break;
					default:
				}

			});
			
			
			/** */
			function searchResults(query) {
				$timeout.cancel(scope.searchTimeout);
				
				if (query !== '') {
					scope.searchTimeout = $timeout(function() {
						elementSpinner[0].style.display = 'block';
						scope.search(query)
							.then(function(data) {
								scope.results = data;
								elementResults[0].style.display = 'block';
								elementSpinner[0].style.display = 'none';
							});
					}, 1000);
				}
				
			}
			
			/** */
			function selectItem(item) {
				scope.query = item.translation.name || item.translation.names;
				scope.model = item;
				elementResults[0].style.display = 'none';
				clear();
			}
			
			/** */
			function focusLost() {
				setTimeout(function() {
					elementResults[0].style.display = 'none';
					if ( scope.results.length ) {
						scope.$apply(function() {
							scope.query = scope.results[0].translation.name || scope.results[0].translation.names;
						});
					} 
				}, 500);				
			}
			
			/** */
			function clear() {
				scope.results = [];
			}		
		}
	};
}

export default Autocomplete;