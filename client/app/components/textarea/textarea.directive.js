import TextareaFieldTemplate from './textarea.html';

function TextareaField() {
	'ngInject';

	return {
		restrict: 'E',
		'template': TextareaFieldTemplate,
		'transclude': true,
		'scope': {
			'name': '@',
			'model': '=',
			'required': '@'
		}
	};
}

export default TextareaField;