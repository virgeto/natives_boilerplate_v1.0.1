import TextareaField from './textarea.directive';

angular.module('Components')
	.directive('textareaField', TextareaField);