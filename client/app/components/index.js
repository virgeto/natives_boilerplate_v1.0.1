angular.module('Components', []);

// Auth
require('./auth');
require('./auth-interceptor');
require('./user');

// Elements
require('./top-menu');
require('./sidebar');
require('./section-label');
require('./login');
require('./material-input');
require('./loader');
require('./confirm-dialog');
require('./content');
require('./image-upload');
require('./image-upload-base64');
require('./validator');
require('./locale');
require('./textarea');
require('./check-box');
require('./autocomplete');
require('./drop-down');