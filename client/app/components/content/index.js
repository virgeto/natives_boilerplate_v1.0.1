import Content from './content.directive';

angular.module('Components')
	.directive('content', Content);