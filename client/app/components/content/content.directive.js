import ContentTemplate from './content.html';

function Content() {
	'ngInject';
	
	return {
		restrict: 'E',
		'template': ContentTemplate,
		'transclude': true,
		'scope': {}
	};
}

export default Content;