export default function ($translateProvider) {
	'ngInject';

	$translateProvider
		.translations('bg', {
			'USERNAME': 'Потребителско име',
			'PASSWORD': 'Парола',
			'OLD_PASSWORD': 'Стара паролата',
			'CREATE': 'Създаване на ',
			'SHOW': 'Редакция на ',
			'CMS': 'aдминистративен панел',
			'LABEL': {
			},
			'HOME': {
				'LABEL': 'Начало'
			},
			'LOG_OUT': {
				'LABEL': 'Изход'
			},
			'LOGIN': {
				'LABEL': 'Вход'
			},
			'MESSAGES': {
				'ENTER_VALID': 'Въведете валидна стойност за  ',
				'IS_REQUIRED': ' задължително поле',
				'CHOOSE_IMAGE': 'Изберете изображение',
				'IMAGE': 'Изберете изображение',
				'IMAGE_REQUIRED': 'Изображението е задължително',
				'NO_RESULTS': 'Не са намерени резултати!',
				'WENT_WRONG': 'Възникна грешка',
				'CATEGORY_CREATED': 'Категорията беше създадена успешно!',
				'UPDATE_SUCCESS': 'Промените бяха записани успешно',
				'ORGANIZER_CREATED': 'Учасващият бе успешно създаден!',
				'DELETE': 'Записът бе успешно изтрит',
				'SURE': 'Сигурни ли сте?',
				'POSITION_CREATE': 'Позицията беше успешно създадена',
				'PASSWORD_CHANGED': 'Паролата бе успешно сменена',
				'PASSWORD_MATCH': 'Новата и старата Ви парола са еднакви!',
				'EXIST_POSITION': 'Избраната позиция вече съществува!',
				'DATE_EXIST': 'Тази дата или време вече съществува',
				'NO_ORGANIZER': 'Не сте избрали участник!',
				'RECORD_EXIST': 'Този запис вече съществува',
				'EVENT_CREATED': 'Събитието бе създадено успешно',
				'SELECT_DATE': 'Изберете дата ...',
				'HOUR_MINUTES': 'Моля изберете час и минути!'
			},
			'BUTTONS': {
				'SAVE': 'Запиши',
				'BROWSE': 'Избери',
				'LOGIN': 'Вход',
				'EDIT': 'Редакция',
				'DELETE': 'Изтрий',
				'REMOVE_DATE_TIME': 'Премахни',
				'ADD_DATE_TIME': 'Добави',
				'ADD_CAST': 'Добави',
				'REMOVE_CAST': 'Премахни',
				'SEARCH': 'Тръси'
			},
			'LABELS': {
				'COUNTRY': 'Държава',
				'NAME': 'Име',
				'USER_NAME': 'Потребителско име',
				'EMAIL': 'Имейл',
				'PASSWORD': 'Парола',
				'PASSWORD_REPEAT': 'Повтори парола'
			}
		})
		.translations('en', {

		});
		
	$translateProvider.useSanitizeValueStrategy(null);
	$translateProvider.use('bg');
}