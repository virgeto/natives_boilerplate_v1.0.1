let apiUrl;

if (__PRODUCTION__) {
	apiUrl = 'http://smart-tv-api.asterart.com/api';
} else if (__STAGING__) {
	apiUrl = 'http://smart-tv-api.asterart.com/api';
} else if (__DEV__) {
	apiUrl = 'http://smart-tv-api.asterart.com/api';
} else {
	apiUrl = 'http://smart-tv-api.asterart.com/api';
}

angular.module('Boilerplate', [
	'ui.router',
	'ngCookies',
	'720kb.datepicker',
	'Components',
	'ngFileUpload',
	'ngAnimate', 
	'toastr',
	'duScroll',
	'naif.base64',
	'base64',
	'bw.paging',
	'pascalprecht.translate'
])
  .config(mainConfig)
	.run(runConfig)
	.constant('AppConstants', {
		'serverUrl': '',
		'apiUrl': apiUrl,
		'languages': ['bg', 'en'],
		'take': 10 // Used to set request tekae parameter UI setting max take = 10 items;
	});

// Configuration
function mainConfig($httpProvider, $urlRouterProvider, $locationProvider, $stateProvider, $translateProvider ) {
	'ngInject';
	
	$httpProvider.interceptors.push('authInterceptor');
	
	$urlRouterProvider.otherwise('/home');
	$locationProvider.html5Mode(true);
	$stateProvider
		.state('app', {
			abstract: true,
			template: '<div ui-view></div>'
		});
	$translateProvider.preferredLanguage('bg');
}

function runConfig(Auth, $rootScope, $state, LoaderComponent) {
	'ngInject';

	$rootScope.$on('$stateChangeStart', function (event, next) {
		window.scroll(0, 0);
		LoaderComponent.spinner('on');

		// Used when route Me is available
		// Auth.isLoggedInAsync(function() {
		// 	$rootScope.userMe = Auth.getCurrentUser();
		// });
		
		if (next.url === '/admin') {
			event.preventDefault();
			$state.go('app.private.login');
		}
	});
	
	$rootScope.$on('$stateChangeSuccess', function () {
		LoaderComponent.spinner('off');		
	});
}