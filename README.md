# Front-end boilerplate

**Dependecies** :
 node 0.12.7
 
**How to run project**:
 
 1. You have to install webpack and webpack-dev-server globaly;
 
    `npm i webpack -g
     npm i webpack-dev-server -g`
 
  
 2. Node-sass module have to be instaled in project directory;
    
    `npm i node-sass`
    
 3. Start webpack server.
  
    `webpack-dev-server`
	
**How to compile project with webpack enviroments**
 		
	 - Development :
	 
	    npm run compile_dev

	 - Staging: 
	 
	   npm run compile_staging
	 
	 - Production:  
	 
	   npm run compile_production
		 

