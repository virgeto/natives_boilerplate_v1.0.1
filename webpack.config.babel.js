const path = require('path');
const webpack = require('webpack');
const hot = new webpack.HotModuleReplacementPlugin();
const env = process.env.NODE_ENV || 'development';
console.log('Compiling for ' + env + ' enviroment.');

const definePlugin = new webpack.DefinePlugin({
	__DEV__: JSON.stringify(JSON.parse(!process.env.NODE_ENV || process.env.NODE_ENV === 'development' ? true : false)),
	__STAGING__: JSON.stringify(JSON.parse(process.env.NODE_ENV === 'staging' ? true : false)),
	__PRODUCTION__: JSON.stringify(JSON.parse(process.env.NODE_ENV === 'production' ? true : false))
});

if (process.argv.indexOf('-p') !== -1) {
	console.log('Production mode, ON!');
}

const config = {
	entry: {
		app: path.resolve('./client/entry.js'),
		vendor: [
			'angular',
			'angular-ui-router',
			'angular-resource',
			'angular-cookies',
			'angularjs-datepicker',
			'ng-file-upload',
			'angular-animate',
			'angular-toastr',
			'angular-translate',
			'angular-scroll',
			'angular-paging',
			'angular-base64/angular-base64',
			'angular-base64-upload',
			'font-awesome/css/font-awesome.min.css',
			'angular-toastr/dist/angular-toastr.css',
			'angularjs-datepicker/dist/angular-datepicker.min.css'
		]
	},
	output: {
		path: path.resolve('./client/build'),
		filename: 'bundle.js',
		publicPath: 'build/'
	},
	module: {
		loaders: [
      { test: /\.scss$/i, 
				loader: 'style!css!autoprefixer?{browsers:["last 2 versions", "Safari >= 8", "Firefox >= 38", "Explorer >= 11"]}!sass',
				exclude: /node_modules/
			}, { 
				test: /\.css$/i, 
				loader: 'style!css!autoprefixer?{browsers:["last 2 versions", "Safari >= 8", "Firefox >= 38", "Explorer >= 11"]}',
			}, {
				test: /\.html$/,
				loader: 'html'
			}, {
				test: /\.js$/,
				loader: 'ng-annotate?add=true!babel?presets[]=es2015',
				exclude: /node_modules/
			}, {
				test: /\.(jpe?g|png|gif)$/i,
				loader: 'url?limit=10000!img?progressive=true'
			}, {
				test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
				loader: 'url?limit=10000&minetype=application/font-woff'
			}, {
				test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
				loader: 'file-loader?name=[path][name].[ext]'
			}
		],
		preLoaders: [
			{
				test: /\.js$/,
				loader: 'eslint-loader',
				exclude: /node_modules/
			}
		]
	},
	plugins: [
		hot,
		definePlugin,
		new webpack.optimize.CommonsChunkPlugin(/* chunkName= */'vendor', /* filename= */'vendor.bundle.js')
	],
	devServer: {
		hot: true,
		contentBase: './client',
		stats: {
			chunkModules: false,
			colors: true,
			progress: true
		},
		historyApiFallback: true
	}
};

module.exports = config;